package pl.wlodarczyk.generatingjwt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GeneratingjwtApplication {

    public static void main(String[] args) {
        SpringApplication.run(GeneratingjwtApplication.class, args);
    }

}
