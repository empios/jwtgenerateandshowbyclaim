package pl.wlodarczyk.generatingjwt;


import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GenerateJWT {

    @GetMapping("/generate")
    public String GenerateJWT(){
        Algorithm algorithm = Algorithm.HMAC256("test123");
        return JWT.create().withClaim("admin", "yes").sign(algorithm);
    }

    @GetMapping("/show")
    public String ShowJWT(){
        DecodedJWT decode = JWT.decode("eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhZG1pbiI6InllcyJ9.WOYe2tuLHWYhYh-UiYKZ4birIwZlmqEopDPhswRPtx4");
        return decode.getClaim("admin").asString();
    }
}
